RCOMP 2019-2020 - Planeamento Sprint 2
===========================================
### Sprint master: 1180586 ###

# 1. Sprint's backlog #
Os membros do grupos terão de realizar a representenção e desenvolvimento da layer 2 e 3 do seu edifício e do backbone, atribuído no Sprint 1. O aluno com o edifício A terá de no fim realizar a integração de todos os trabalhos realizados no Packet Tracer numa única simulação.
Na sua representação do projeto, cada aluno terá de utilizar os dispositivos apresentados no enunciado e será responsável pela criação de uma VLAN Database no seu edifício e da definição dos IPs que irá utilizar no seu projeto individual.


# 2. Decisões técnicas #
  * VLAN default = 1 (para todos os edifícios).
  * VLAN IDs inutilizadas: 131 - 150
  * VTP Domain: vtdgb2
  * Identificação dispositivos: Edificio_Piso_Dispositivo (p.e. EDA_1_MC, EDC_0_HC1).
  * Número total de VLANIDs (5 * 4) + 1 = 21.
  * Bloco de endereços IPv4: 10.165.112.0/20
  * Endereço router ISP: 17.10.10.89/30
  * A divisão dos IPs pelos diferentes membros encontra-se num Excel (um individual em cada pasta e um global).

| Nome VLAN |  VLAN ID  |
|------------|-----------|
| 110 | VLAN_B_F0 |
| 111 | VLAN_B_F1 |
| 112 | VLAN_B_WIFI |
| 113 | VLAN_B_VOIP |
| 114 | VLAN_B_SERVER |
| 115 | VLAN_A_F0 |
| 116 | VLAN_A_WIFI |
| 117 | VLAN_A_F1 |
| 118 | VLAN_A_VOIP |
| 119 | VLAN_A_Server |
| 120 | VLAN_D_F0 |
| 121 | VLAN_D_F1 |
| 122 | VLAN_D_WIFI |
| 123 | VLAN_D_VOIP |
| 124 | VLAN_D_SERVER |
| 125 | VLAN_C_F0 |
| 126 | VLAN_C_F1 |
| 127 | VLAN_C_WIFI |
| 128 | VLAN_C_VOIP |
| 129 | VLAN_C_SERVER |
| 130 (Backbone) | VLAN_BB |

# 3. Atribuição de Tarefas #
  * 1180586 - Desenvolvimeento da layer2 e 3 no Packet Tracer do Edifício A e junção de todas as simulações.
  * 1181488 - Desenvolvimeento da layer2 e 3 no Packet Tracer do Edifício B.
  * 1181486 - Desenvolvimeento da layer2 e 3 no Packet Tracer do Edifício C.
  * 1180589 - Desenvolvimeento da layer2 e 3 no Packet Tracer do Edifício D.
