# Edifício B - Daniel Dias 1181488

Neste projeto o Edifício B foi criado, tal como os outros edifícios, tendo como base o projeto realizado no Sprint 1.
Os Switches (PT-EMPTY SWITCHES) representados tem a finalidade de simular os HCs, ICs, MCs, e CPs (os CP's foram substituidos no piso 0 pelo IC e no piso 1 pelo HC,por questões de organização).Também foram estabelecidas as devidas ligações aos dispositivos de layer 3 (2 PCs em cada piso de modo a facilitar verificações de conexão, Laptop, Server e IPhone).
A cablagem usada neste projeto correspondente à utilizada no Sprint 1.
A conexão por cabo do Router com o Intermediate Cross Connect (em modo Trunk) permitiu que se encontrassem nesse dispositivo de rede apenas as VLAN ID's e respetivos endereços que fossem cruciais para a execução de uma ligação do Switch ao End User, permitindo assim uma maior percepção da sua localização e transmissão.
Para fazer uma representação que permitisse posteriormente uma ligação mais ágil e efetiva dos backbones dos diferentes edifícios ao MCC do edifício B, realizámos uma ligação do switch representativo do MCC aos diferentes ICCs dos edifícios.

## Representação

![EDB](EDB.JPG)

## Material utilizado

6 Switch PT-Empty (com portas PT-SWITCH-NM-1FGE para fibra e PT-SWITCH-NM-1CFE)
4 Router 2811
1 Access-Point (AP-PT)
4 PCs
1 IP-Phone (7960)
1 Server
1 Laptop

## Distribuição e utilização de endereços

A distribuição dos endereços encontra-se no Excel da pasta do Edifício B.
