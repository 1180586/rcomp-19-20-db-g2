# Edifício D - Eduardo Remelhe - 1180589
O edifício D, neste projeto foi criado, tal como os outros edifícios, tendo como base o projeto realizado no Sprint 1.
Primeiramente foram utilizados switches de modo a simular os MCs, ICs, HCs e CPs necessários (foram utilizados 3 CPS de modo a simbolizar o total de CPs usados no Sprint1 apenas por motivos estéticos e de organização) e num instante seguinte estabeleceram-se as devidas ligações aos dispositivos de layer 3 (6 PCs de modo a facilitar verificações de conexão, Laptop, Server e Phone).
Toda a cablagem foi mantida de modo a corresponder ao Sprint anterior mas neste Sprint utilizei cabos Copper Straight-Through para ligar Switchs e End-Devices e cabos Copper Cross-Over.
A utilização de um router ligado ao EDD_IC (Switch representativo do IC) permitiu que se encontrassem condensadas nesse dispositivo de rede apenas as VLAN IDs e respetivos endereços que fossem importantes para a realização de uma ligação Switch - End User, permitindo uma maior noção da sua localização e transmissão. Desta forma, ao realizar as interligações dos switches em modo trunk, a propagação dessas VLANs era assegurada permitindo acessar aos dispositivos aos quais se pretendia estabelecer ligação.
Para fazer uma representação que permitisse posteriormente uma ligação mais ágil e efetiva dos backbones dos diferentes edifícios ao MCC do edifício D, realizámos uma ligação do switch representativo do MCC aos diferentes ICCs dos edifícios.

##Dispositivos
10 Switch PT-Empty com entradas: PT-SWITCH-NM-1FGE (1 MC, 4 IC, 2 HCs e 3 CPs)
4 Router 2811
6 PC
1 IP-Phone
1 Server
1 Access-Point
1 Laptop

##Endereços IPV4 e VLANS
Localizados no Excel:IPV4_D na correspondente pasta.

##Representação
![EDD](EDD.JPG)
