# Edificio C - Carlos Daniel Santos Pinto Ferreira - 1181486

De forma a priorizar a coesão do projeto, o edifício C respeita todas as decisões tomadas no sprint anterior. Desta feita, todas as ligações realizadas com cabos de cobre e fibra foram respeitadas e mantidas no presente projeto.

Numa instancia inicial da realização do projeto, tomei como iniciativa omitir todos os CP's representados no Sprint anterior de forma a garantir uma apresentação simplificada e esteticamente mais minimalista do Edificio em questão.

Assim sendo, utilizei PT-EMPTY SWITCHES de forma a representar os Cross-Connects (Main Cross Connect, Intermediate Cross Connect, Horizontal Cross Connect) e os Consolidation Points.

Desta feita, os PT-EMPTY SWITCHES denominados por EDC_1_CP1, EDC_1_CP2, EDC_1_CP3 tem como função, simular todos os Consolidation Points do Edificio C.

A estes Switches, efetuei ligações aos diferentes dispositivos requiridos no enunciado (PC's, Laptop's, IPPhone's, Server's).

A utilização de um Router ligado ao Intermediate Cross Connect do presente Edificio, permitiu que se encontrassem nesse dispositivo de rede apenas as VLAN ID's e respetivos endereços que fossem cruciais para a execução de uma ligação do Switch ao End User, permitindo assim uma maior percepção da sua localização e transmissão.

Dessa forma, ao executar as ligações dos Switches em Trunk Mode, a propagassão dessas VLAN's por todos os outros Switches era asseguranda. Permitindo assim acessar aos dispositivos dos quais se pretendia fazer ligação.

Realizei uma ligação do Main Cross Connect, representado por um Switch, aos diferentes Intermediate Cross Connects dos diversos edifícios de forma a simular uma representação fidedigna de uma ligação ágil e efetiva entre os diferentes edifícios.


## Representação ##

![EDC](EDC.JPG)

## *VLAN's* ##

No presente edificio foram consideradas as seguintes VLAN's:

| Nome VLAN | Numero VLAN |
|:---------:|:-----------:|
|VLAN_C_F0 | 125|
|VLAN_C_F1 | 126|
|VLAN_C_WIFI| 127|
|VLAN_C_VOIP| 128|
|VLAN_C_SERVER| 129|


### *Piso 0* ###

No presente piso foram considerados os seguintes dispositivos com os seguintes IP's:

| Nome dispositivo |  Endereço | Mascara | Default Gateway|
|:----------------:|:---------:|:-------:|:--------------:|
| EDC_0_PC1 | 10.165.117.2 | 255.255.255.192 | 10.165.117.1 |
| EDC_0_PC2 | 10.165.117.3 | 255.255.255.192 | 10.165.117.1|
|EDC_0_IPPhone1| - | - | - |
|EDC_0_AC| - | - | - |
| EDA_1_Server2 | 10.165.116.2 | 255.255.255.0 | 10.165.116.1|
| EDC_o_Laptop| 10.165.117.130 | 255.255.255.192 | 10.165.117.129|


### *Piso 0* ###

No presente piso foram considerados os seguintes dispositivos com os seguintes IP's:


| Nome dispositivo |  Endereço | Mascara | Default Gateway|
|:----------------:|:---------:|:-------:|:--------------:|
|EDC_1_PC1 | 10.165.117.66 | 255.255.255.192 |10.165.117.65|
|EDC_1_PC2 | 10.165.117.67 | 255.255.255.192 |10.165.117.65|
|EDC_1_PC3| 10.165.117.68 | |255.255.255.192 | 10.165.117.65|
|EDC_1_PC4| 10.165.117.69 | 255.255.255.192 | 10.165.117.65|
|EDC_1_PC5| 10.165.117.70 | 255.255.255.192| 10.165.117.65|


### Inventário ###
   * 10 PT-EMPTY Switches (Com Portas PT-SWITCH-NM-1FGE para conexão com cabo de Fibra e PT-SWITCH-NM-1CFE para conexão com cabo de Cobre)
   * 4 Router 2811
   * 1 Access-Point (AP-PT-N)
   * 7 Desktops (PC-PT)
   * 1 Telefone (IPPhone)
   * 1 Servidor (Server-PT)
   * 1 Laptop (Laptop-PT)

## Distribuição e Utilização de endereços IPV4
A representação dos endereços IPV4 encontram-se no ficheiro Excel no interior da Pasta referente ao Edificio C
