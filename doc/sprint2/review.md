RCOMP 2019-2020 Project - Sprint 2 review
=========================================
### Sprint master: 1180586 ###
# 1. Sprint's backlog #
Todos os membros do grupos terão de realizar a representenção e desenvolvimento da layer 2 e 3 do seu edifício e do backbone, atribuído no Sprint 1. O aluno com o edifício A terá de no fim realizar a integração de todos os trabalhos realizados no Packet Tracer numa única simulação. Os dispositivos utilizados durante a realização do projeto devem ser as mesmas por todos os membros do grupo e devem respeitar as condições apresentadas no enunciado do Sprint. Procedeu-se à divisão das VLANs e IPv4 durante o processo de planning, de forma a promover uma melhor distribuição do trabalho pelos membros da equipa.

# 2. Subtarefas #
## 2.1. 1180586 - Representação da estrutura cablada Edifício A #
### Implementado sem problemas. ###
O trabalho foi feito de uma forma sequencial, de forma a que fosse possível testar cada um dos dispositivos nas diferentes layers. Como forma de melhorar poderia ter verificado melhor os constituintes da ligação à cloud, uma vez que foi algo difícil de entender no momento.

## 2.2. 1181488 - Representação da estrutura cablada Edifício B #
### Implementado sem problemas. ###
Teria tentado respeitar ao máximo a cablagem realizada no Sprint 1.

## 2.3. 1181486 - Representação da estrutura cablada Edifício C #
### Implementado sem problemas. ###
Todo o processo de realização do trabalho foi estável e de evolução constante. 

## 2.4. 1180589 - Representação da estrutura cablada Edifício D #
### Implementado sem problemas. ###
Sinto que o trabalho correu em boas condições, senti evolução ao longo do mesmo. Talvez tentasse atribuir um número mais realístico de PCs e CPs de forma a simular o edifício de forma mais imersiva.
