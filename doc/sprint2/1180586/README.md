# Edifício A - Catarina Rodrigues - 1180586
O edifício A, neste projeto foi criado tendo como base o projeto realizado no Sprint 1. Tendo isto em conta, todas as ligações realizadas em cabos de cobre e fibra foram mantidas neste mesmo projeto. 

O projeto foi realizado tendo em conta a ligação à internet, a ligação aos edifícios constituintes do campus e a construção da rede de dispositivos dos pisos 0 e 1 do edifício.

Num primeiro instante procedi à instalação dos Cross-Connects anteriormente posicionados, aos quais estabeleci ligações aos diferentes dispositivos pedidos no enunciado (PC, Laptop, IP-Phone e Servidor). De forma a realizar o ping entre endereços de dispositivos da mesma VLAN optei por colocar 2 PCs para facilitar a visualização da simulação, assim como verificar a minha configuração da layer 2 e posteriormente, a configuração da layer 3, realizando pings de routers com dispositivos.

A instalação das partes constituintes da ligação à internet (DSL, Cloud e ISP-Router) foi realizada tendo em conta que o ISP-Router seria o detentor da informação relativa ao endereço ISP. Ao estabelecer a ligação à internet no edifício A, tive em conta o facto desta necessitar de depois ser propagada para os restantes dispositivos na rede do campus.

A utilização de um router ligado ao EDA_1_ICC (Switch representativo do ICC) permitiu a configuração das VLAN IDs e respetivos endereços que fossem importantes para a realização de uma ligação Switch - End User, permitindo uma melhor perceção da transmissão da rede. Desta forma, ao realizar as interligações dos switches no modo trunk, a propagação dessas VLANs era assegurada permitindo estabelecer ligação com os dispositivos pretendidos.

A ligação entre switches foi realizada utilizando 2 cabos (no meu caso de fibra ótica), de forma a providenciar caminhos alternativos no caso de alguma falha de ligações (STP). Ao estabelecer ligações redundantes entre computadores, um dos caminhos vai ser visto como sendo o mais "eficiente" e, no caso de alguma falha, será utilizado um novo caminho mais eficiente de maneira a conseguir continuar com a ligação.

Para fazer uma representação que permitisse posteriormente uma ligação mais ágil e efetiva dos backbones dos diferentes edifícios ao MCC do edifício A, realizámos uma ligação do switch representativo do MCC aos diferentes ICCs dos edifícios. Desta forma conseguimos realizar testes de ping entre routers para verificar o routing de cada um.

## Representação
![EDA](EDA.JPG)

## Material utilizado
7 Switch PT-Empty (com portas PT-SWITCH-NM-1FGE para fibra e PT-SWITCH-NM-1CFE)
6 Router 2811
1 Access-Point (AP-PT)
1 Cloud-PT
1 DSL-Modem-PT
4 PCs
1 IP-Phone (7960)
1 Server
1 Laptop

## Distribuição e utilização de endereços
A distribuição dos endereços encontra-se no Excel da pasta do Edifício A.

## Observações relativamente ao Sprint anterior
A realização do trabalho regiu-se pelo trabalho realizado no Sprint 1. Depois de analisar os comentários realizados anteriormente no Sprint 1, foi colocado um Access-Point de forma a poder permitir a distribuição WIFI pelo edifício.