# Edifício A - Catarina Rodrigues 1180586

Os cabos utilizados no edifício assim como no exterior são de cobre CAT6A (amarelo) ou de fibra ótica (azul). 
A quantidade de fibra ótica utilizada só foi usada de forma a possibilitar a conexão das diferentes partes que constituem o backbone (ligação entre MC, IC e HCs). Desta forma o encargo a nível monetário da empresa não será elevado, uma vez que normalmente as empresas não se encontram equipadas com o hardware que permita a instalação deste tipo de fibra pois também é de difícil conexão e instalação. Tendo em conta que o no exterior é suposto realizar a ligação entre os diferentes edifícios a utilização do tipo monomode é mais aconselhável, devido à área que necessita de ser coberta.
A utilização de cabos de cobre CAT6A permite uma melhor conexão que o CAT6 pois o seu revestimento tem melhor resistência à possibilidade de radiação eletromagnética (comum quando existem interferências entre cabos). O facto de apresentar cabos cruzados, uma blindagem de alumínio e uma capacidade de 10Gbps torna-o uma escolha mais sustentável e com uma qualidade semelhante ao CAT7, a um menor preço.

## Piso 0
![Piso0](Piso0.png)

## Legenda
![Legenda](Legenda.JPG)

### Escala
* A escala utilizada para realizar as medições foi de 4cm no papel, o que é equivalente a 10m no real.

### Cablagem estruturada utilizada
* Neste piso é apenas utilizado um Horizontal Cross-Connect. Este HC necessitará de um patch panel de 48 entradas para os outlets + 48 switches = 96 (patch panel 4U), ao qual acrescentamos 24 entradas para os outlets + 24 switches = 48 (patch panel 2U), uma vez que o número total de outlets no piso é de 51. Desta forma, ainda há possibilidade de acrescentar equipamento (switches, outlets, entre outros), deixando ainda 21 portas para ser utilizadas.

### Dimensões e outlets:
* Espaço A0.1 - área de trabalho que apenas necessitará de 5 outlets na mesa castanha.
* Espaço A0.2 - área útil com 132,2m2, necessita de 28 outlets.
* Espaço A0.3 - área útil com 85,2m2 necessita de 18 outlets.

### Cablagem - Piso 0
* Espaço A0.1 (CAT6A)
	* distância mínima - 1m (0,2+0,1+0,1 = 0,4cm no papel) 
	* distância máxima - 12,25m (0,2+3,7+1,0 = 4,9cm no papel)
	* total de cabos no espaço - 119,34m (média do cabo = 6,63m).

* Espaço A0.2 (CAT6A e fibra ótica)
	* distância mínima de fio de cobre - 2,5m (0,3+0,6+0,1 = 1,0cm no papel).
	* distância máxima de fio de cobre - 10,25m (0,8+3,2+0,1 = 4,1cm no papel).
	* total de cabos de cobre no espaço - 178,64‬m (média do cabo = 6,38m).
	* total de cabos de fibra ótica no espaço - 5m (1,5+0,5 = 2cm no papel).

* Espaço A0.3 (CAT6A)
	* distância mínima - 2,5m (0,6+0,3+0,1 = 1,0cm no papel). 
	* distância máxima - 10,75m (0,6+3+0,7 = 4,3 cm no papel).
	* total de cabos no espaço - 33,15‬m (média do cabo = 6,63m).

* Áreas comuns (passagem de cabos entre salas - CAT6A)
	* distância total - 50m (11,2+4,7+0,1+4,0 = 20cm no papel).


## Piso 1
![Piso1](Piso1.JPG)

## Legenda
![Legenda](Legenda.JPG)

### Escala
* A escala utilizada para realizar as medições foi de 4,1cm no papel, o que é equivalente a 10m no real.

### Cablagem estruturada utilizada
* Neste piso encontra-se o Intermediate Cross-Connect que possibilita a ligação dos 2 Horizontal Cross-Connects e que por si só irá ligar ao Main Cross-Connect que permitirá fazer a ligação entre os edifícios.

* O HC necessitará de um patch panel de 48 entradas para os outlets + 48 switches = 96 (patch panel 4U), ao qual acrescentamos 24 patch pannels + 24 switches = 48 (patch panel 2U), uma vez que o número total de outlets no piso é de 54. Desta forma, ainda há possibilidade de acrescentar equipamento (switches, outlets, entre outros), deixando ainda 18 portas para ser utilizadas.

* O Intermediate Cross-Connect e o Main Cross-Connect vão necessitar de um patch panel 2U (cada um) para realizar a ligação entre os diferentes componentes da backbone deste edifício. 

### Dimensões e outlets:
* Espaço A1.1 - datacentre, por isso não terá outlets.
* Espaço A1.2 - área útil com 43,76m2, necessita de 10 outlets.
* Espaço A1.3 - área útil com 81,02m2, necessita de 18 outlets.
* Espaço A1.4 - área útil de 125,7m2, necessita de 26 outlets.

### Cablagem - Piso 1
* Espaço A1.1 (fibra ótica)
	* total de cabos no espaço - 18,54m (4,6+3,0 = 7,6cm no papel).

* Espaço A1.2 (CAT6A)
	* distância mínima - 2,93m (0,1+0,6+0,5 = 1,2cm no papel).
	* distância máxima - 9,27m (0,1+2,7+0,5+0,5 = 3,8 cm no papel). 
	* total de cabos no espaço - 61m (média do cabo = 6,1m).

* Espaço A1.3 (CAT6A)
	* distância mínima - 1,7m (0,5+0,2= 0,7cm no papel). 
	* distância máxima - 11,95m (0,8+3,9+0,2= 4,9 cm no papel). 
	* total de cabos no espaço - 122,94‬‬m (média do cabo = 6,83m).

* Espaço A1.4 (CAT6A)
	* distância mínima - 4,39m (0,5+1,3 = 1,8cm no papel). 
	* distância máxima - 15,37m (2,0+4,0+0,3 = 6,3 cm no papel).
	* total de cabos no espaço - 256,88‬‬m (média do cabo = 9,88m).

* Áreas comuns (passagem de cabos entre salas - fibra ótica)
	* distância total - 52,44m (11,1+7,4+3,0+12,0 = 21,5cm no papel).

* Paredes (passagem de cabos entre pisos - fibra ótica)
	* distância total - 12m (parede do piso 0 com 4m de altura).


# Área Exterior
![Exterior](Exterior.JPG)

## Legenda
![Legenda](Legenda.JPG)

### Escala
* A escala utilizada para realizar as medições foi de 1,2cm no papel, o que é equivalente a 20m no real.

### Cablagem
* Na vala técnica são necessários 820m de cabos de fibra ótica (2,2+13,5*2+9,5*2+0,2*5 = 49,2m no papel) para realizar a alimentação de todos os edifícios.


# Inventário
* 821,95m de fio de cobre (381,13m piso 0 + 440.82 piso 1).
* 907,98m de fibra ótica (5m ligação piso 0 + 12m ligação dos 2 pisos + 52,44m ligação piso 1).
* 105 outlets (51 no piso 0 + 54 no piso 1).
* 1 Main Cross-Connect.
* 1 Intermediate Cross-Connect.
* 2 Horizontal Cross-Connect (1 para cada piso).
* 2 Patch Panels 4U (1 para cada piso) e 4 Patch Panels 2U (2 para os HCs e 1 para MC e IC)