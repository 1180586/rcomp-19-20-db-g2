RCOMP 2019-2020 - Sprint 1
=========================================
### Sprint master: 1181486 ###
# 1. Sprint's backlog #
Neste trabalho era necessária a criação de um plano com uma estrutura de cablagem, envolvendo também todas as partes constituintes e necessárias para a realização do mesmo.
Em cada edifício tivemos de realizar um plano de montagem de cabos que não só permitissem a ligação entre os diferentes edifícios mas também a ligação entre os diferentes constituintes de cada um (p.e. ligação entre MCs, HCs, ICs, CPs, outlets, entre outros). Também tivemos em atenção todo o material que seria necessário de instalar numa situação real como switches, patch panels, cabos de cobre e de fibra ótica.

# 2. Subtarefas #
## 2.1. 1180586 - Estrutura cablada Edifício A #
### Implementado sem problemas. ###

Apesar do trabalho ter sido feito de uma forma progressiva e gradual, com melhoramentos ao longo do percurso sinto que poderia ter tido mais em conta a especificação da distância user e outlet (3m) que em certas salas não foi totalmente respeitada, de forma a dar uma visão mais realista.

## 2.2. 1181488 - Estrutura cablada Edifício B #
### Implementado sem problemas. ###

Teria adotado outro método para medir o cabo utlizado que não fosse a impressão e medição pelo papel.

## 2.3. 1181486 - Estrutura cablada Edifício C #
### Implementado sem problemas. ###

Todo o processo de realização do trabalho foi estável e de evolução constante. No entanto, algumas coisas poderiam ter sido otimizadas ao nivel da planta e respectiva exibição de outlets, calhas, cabos.
Creio que poderia ter colocado no trabalho que 1 outlet correspondia a 2 outlets na realidade. Desta forma, seria de mais facil interpretação e visualização da posição de cada respetivo outlet.
A nivel de relatório, creio que um orçamento seria um toque que iria valorizar todo o processo realizado.

## 2.4. 1180589 - Estrutura cablada Edifício D #
### Implementado sem problemas. ###

Apesar do trabalho ter sido realizado sem grandes problemas ou dificuldades, há sempre algo a melhorar, tendo em conta que estamos a trabalhar com medições que por si só é uma área muito problemática a nível de erros, basta um simples erro de medição e podem ser comprometidas várias regras de distribuição de outlets, alcance de redes WIFI LAN, entre outros.
