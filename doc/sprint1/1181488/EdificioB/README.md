# Edifício B - Daniel Dias 1181488


Os cabos usados nas ligações no edifício e na ligação com o exterior são cobre CAT6A (vermelho) e fibra ótica (azul) respetivamente. A quantidade de fibra ótica utilizada só foi usada de forma a possibilitar a conexão das diferentes partes que constituem o backbone (ligação entre MC, IC e HCs). Desta forma o encargo a nível monetário da empresa não será elevado, uma vez que normalmente as empresas não se encontram equipadas com o hardware que permita a instalação deste tipo de fibra pois também é de difícil conexão e instalação.
Todo o edificio tem cobertura total de Wifi LAN.
A utilização de cabos de cobre CAT6A permite uma melhor conexão que o CAT6 pois o seu revestimento tem melhor uma melhor resistência à possibilidade de radiação eletromagnética (comum quando existem interferências entre cabos).


## Piso 0
![x](piso0.PNG)
### Legenda
![z](legenda.PNG)

### Escala
* A escala utilizada para realizar as medições foi de 2.8cm no papel, o que é equivalente a 10m no real.

### Cablagem estruturada utilizada
* O intermidiate cross connect situa-se na entrance facility pois é a primeira sala por qual os cabos de fibra ótica passam.
* Este IC necessitará de um patch panel de 84 entradas para os outlets + 84 switches = 168 (patch panel 7U), ao qual acrescentamos 24 entradas para os outlets + 24 switches = 48 (patch panel 2U), uma vez que o número total de outlets no piso é de 82. Desta forma, ainda há possibilidade de acrescentar equipamento (switches, outlets, entre outros), deixando ainda 26 (108-82) portas para ser utilizadas.
* Os 2 AP’s são de 2.4 GHz, pelo facto de serem mais baratos e ao mesmo tempo atravessam mais facilmente objetos físicos cobrindo assim o maior número de divisões, e estão em canais diferentes para não haver overlapping.
*	Os cabos de cobre são CAT6A, pois permitem uma melhor conexão que o CAT6 pois o seu revestimento tem melhor uma melhor resistência à possibilidade de radiação eletromagnética (comum quando existem interferências entre cabos).
* Usou-se cabo de fibra ótica monomode devido à menor atenuação de sinal e maior propagação de sinal em maiores distâncias. Estes cabos contêm 48 fibras para poderem ser ligados às entradas dos Patch Panels. Neste piso o cabo de fibra otica é distribuido através de uma pista subterrânea.

### Dimensões e outlets:
* Service Desk - área de trabalho, 6 outlets.
* B0.2- área útil com 89,29 m2, 28 outlets.
* B0.3- área útil com 126,02 m2 necessita de 26 outlets.
* B0.4- área útil com 152,55 m2 necessita de 32 outlets.

### Cablagem - Piso 0
* Espaço B0.2 (CAT6A)
	* distancia de cabos de cobre no espaço - 255,42m .

* Espaço B0.3 (CAT6A)
	* distancia de cabos de cobre no espaço - 494.24m .

* Espaço B0.4 (CAT6A)
	* distancia de cabos de cobre no espaço - 992.64m .

* Service Desk (CAT6A)
	* distancia de cabos no espaço - 66.36m.

* Distancia coberta pelos cabos de cobre
	* distância total - 1808.42 m.

* Distancia coberta pelo cabo de fibra ótica
	* distância total - 72.63m.


## Piso 1
![y](piso1.PNG)
### Legenda
![z](legenda.PNG)

### Escala
* A escala utilizada para realizar as medições foi de 2.8cm no papel, o que é equivalente a 10m no real.

### Cablagem estruturada utilizada
* Este HC necessitará de um patch panel de 120 entradas para os outlets + 120 switches = 240 (patch panel 10U), ao qual acrescentamos 24 entradas para os outlets + 24 switches = 48 (patch panel 2U), uma vez que o número total de outlets no piso é de 116. Desta forma, ainda há possibilidade de acrescentar equipamento (switches, outlets, entre outros), deixando ainda 28 (144-116) portas para ser utilizadas.
* Os cabos de cobre são CAT6A, pois permitem uma melhor conexão que o CAT6 pois o seu revestimento tem melhor uma melhor resistência à possibilidade de radiação eletromagnética (comum quando existem interferências entre cabos).
* Usou-se cabo de fibra ótica monomode devido à menor atenuação de sinal e maior propagação de sinal em maiores distâncias. Estes cabos contêm 48 fibras para poderem ser ligados às entradas dos Patch Panels. Neste piso o cabo de fibra otica é distribuido pelo teto através de uma calha, sendo que tem um teto falso com 0.5m.

### Dimensões e outlets:
* B1.1- área útil com 21.05 m2, 6 outlets.
* B1.2- área útil com 35.20 m2, 8 outlets.
* B1.3/B1.4/B1.5- área útil com 41.07 m2, 10 outlets cada, 30 no total.
* B1.6/B1.7/B1.8- área útil com 46.43 m2, 10 outlets cada,30 no total.
* B1.9- área útil com 80.23 m2, 18 outlets.
* B1.10/B1.11- área útil com 55.36 m2, 12 outlets cada,24 no total.

### Cablagem - Piso 0
* Espaço B1.1 (CAT6A)
	* distancia de cabos de cobre no espaço - 34,8m .

* Espaço B1.2 (CAT6A)
	* distancia de cabos de cobre no espaço - 64.8m .

* Espaço B1.3/B1.4/B1.5 (CAT6A)
	* distancia total de cabos de cobre nos espaços - 265.2m.

* Espaço B1.6/B1.7/B1.8 (CAT6A)
	* distancia de cabos de cobre no espaço - 302.1m.

* Espaço B1.9 (CAT6A)
	* distancia de cabos de cobre no espaço - 272.88m.

* Espaço B1.10/B1.11 (CAT6A)
	* distancia de cabos de cobre no espaço - 149.64m.

* Distancia coberta pelos cabos de cobre
	* distância total - 1230.3.42 m.

* Distancia coberta pelo cabo de fibra ótica
	* distância total - 80.69m.

# Inventário:
* 1 intermidiate cross connect
* 1 horizontal cross connect
* 13 consolidation point
* 3 wireless access point
* 198 Outlets		
* 2 Patch Panels 2U, 1 Patch Panel 10U e 1 Patch Panel 7U
* 3038.72 metros de cabo de cobre CAT6A
* 153.32 metros de cabo de fibra ótica monomode
