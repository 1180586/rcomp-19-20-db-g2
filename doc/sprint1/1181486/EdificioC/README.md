# Edifício A - Carlos Daniel Ferreira 1181486

O presente edificio conta com 2 pisos.

  * Piso 0:

    * Entrada do edificio;
    * Cinco salas;
    * Duas casas de banho;
    * Uma área aberta com calhas suspensas

  * Piso 1:

    * Cinco Salas

  * Dimensões de 78.21 x 30.35 metros;
  * Área total de: 2373.67 m^2

## Outlets ##

No presente projeto sempre foram respeitadas duas regras presentes nos padrões de cabeamento estruturado.

  * ##### 2 Outlets/10 m^2: #####
  
      Os padrões de cabeamento estruturado indicam que deve existir no mínimo 2 outlets por cada 10 metros de área. Assimm sendo  o número de outlets foi arredondado por excesso sempre que necessário.

  * ##### 3 metros de distância: #####
  
      Os padrões de cabeamento estruturado indicam que deve existir sempre um outlet no raio de 3 metros a partir de uma especifica posição de trabalho. Ou seja, deve ser sempre possivel alcançar cada outlet de rede a 3 metros de distância.

  A disposição dos outlets por sasla foi feita de acordo com os critérios em cima descritos, tendo também em conta a disposição mais eficaz de forma a não obstruir locais de passagem ou de outra importância.


### *Piso 0* ###

No presente piso foram consideradas as seguintes medidas e os respetivos números de outlets a serem utilizados:


|       Sala       |   C0.1      |  C0.2       |   C0.3  |   C0.4  |  C0.5   |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-------:|:-------:|
|Dimensões C x L (m)| 7.85 x 6.07 | 7.85 x 6.07 | 7.85 x 6.07 | 8.21 x 5.71 | 6.07 x 5.71 |
|Área(m2) | 47.65 | 47.65 |  47.65  | 46.87 | 34.66|
|Outlets | 10   | 10 | 10 | 10 | 8 |


### *Piso 1* ###

No presente piso foram consideradas as seguintes medidas e os respetivos números de outlets a serem utilizados:


|       Sala      |   C1.1      |  C1.2       |   C1.3  |   C1.4  |  C1.5   |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-------:|:-------:|
|Dimensões C x L (m)| 5.94 x 6.75 | 5.94 x 6.75 | 5.94 x 10.81 | 5.94 x 10.81 |12.97 x 5.41 |
|Área(m2) | 40.10 | 40.10 |  64.20  | 79.80 | 70.16|
|Outlets | 10   | 10 | 14 | 16 | 16 |


### Cablagem ###

No currente edifício, para ligar o IC aos HC's foram utilizados cabos de fibra ótica monomodo pois suportam velocidades e distâncias superiores.
Sendo este um fator crucial devido ao volumoso número de utilizadores finais existentes, pensamos ser a melhor opção mesmo tendo um custo superior aos cabos de cobre.

Nas salas, foi decidido a utilização do cabo de cobre CAT6A, pois é um cabo de cobre dos mais recentes do mercado. Este permite também uma velocidade de 10 Gigabits por segundo.
Bas extremidades dos cabos de cobre, foram colocadas pontas macho tipo RJ45. Assim sendo, nos outlets foram colocadas entradas fêmea.

### Access Points ###

O edifício C exigia uma cobertura Wi-Fi total. Desta feita, foram necessários 3 Access Points de diâmetro de 50 metros. Utilizamos 3 canais distintos para não existir interferência de sinal entre os vários sinais Wi-Fi:
 
   * Os canais 1 e 6 npiso 0, da esquerda para a direita.
   * O canal 11 no piso 1.
   
O seu posicionamento foi pensado de modo a existir o menor número de obstáculos possíveis, proporcionando uma melhor propagação de sinal.

Foi também utilizado um cabo direto do HC sem o uso de Consolidation Point a cada Acess Point devido à possibilidade do número de utilizadores poder ser elevado, o que seria prejudicial se o sinal fosse partilhado por outros outlets.
Tivemos atenção ao diâmetro máximo de alcance de cada Access Point (respetivamente 50 metros).
Tendo em conta a existência de teto falso, foi decidido que toda a cablagem correspondente aos Access Points fosse feita por  aí para efeitos de organização, aproveitamento de espaço e estética.


## Piso 0 ##

Todas as ligações de cabo foram feitas até o Horizontal Cross Connect.

Na parte esquerda do edifício, os HC's (Horizontal Cross Connect), CP's (Consolidation Points) e AP's (Access Points) foram colocados a 2.5 metros do solo, enquanto que os outlets estão situados no chão. 
A passagem dos cabos para as outras salas foi feita pelo teto fals, o que levou levou a que existissem furos no teto para que se pudessem passar os cabos.

Na parte direita do edifício (espaço livre) devido à existência de calhas suspensas a 3 metros do chão, a maior parte dos Outlets, CP's e os respetivos cabos foram aí colocados.
O resto foi colocado junto às paredes de maneira a passar os cabos minimizando o número de furos entre salas.

O IC é constituído por uma cabine de 1 rack que contém um switch de fibra de 8 portas que recebe 2 cabos de fibra ótica do MC ( exterior) e liga a cada HC dos pisos através de cabos de fibra ótica.

O HC é constituido por um cabine de 1 rack que contém um switch híbrido de 24 portas que recebe o sinal fibra do IC e distribui um cabo de cobre para cada CP existente.

Foram utilizados 4.1663 km de cabo. No resultado estão considerados o plano de posicionamento dos HC's e CP's.

CP1 – Sala C0.2 – Constituído por uma cabine de dois racks, um patch panel de 24 portas, um switch de 24 portas, 12 metros de patch cords.

CP2 – Sala C0.4 – Constituído por uma cabine de dois racks, um patch panel de 24 portas e um switch de 24 portas, 12 metros de patch cords.

CP3 - Area aberta (Canto Superior) - Constituído por uma cabine de oito racks, 4 patch panels de 24 portas, 4 switches de 24 portas e 45 metros de patch cords.

CP4 - Area aberta (Canto Inferior) - Constituído por uma cabine de seis racks, 3 patch panels de 24 portas, 3 switches de 24 portas e 36 metros de patch cords.

CP 5 - CP 9 - Area aberta (Calha) - Constituído por uma cabine de uma rack 1 patch panel de 24 portas, 1 switch de 24 portas e 23 metros de patch cords.

Relativamente aos Access Poins: 

  * Do Access Point 1 até o Horizontal Cross Connect são 37.37 metros de cabo;
  * Do Access Point 2 são 68.95 metros. Finalizando a cabelagem;

Assim sendo:

  * Piso 0 : 4060 + 37.37 + 68.95 = 41663 metros.


![Building C - Ground Floor](Floor0C.png)


### Legenda ###


![Legenda](Legenda.jpg)


### Inventário ###

  *  Outlet: 394 unidades
  *  Cabine de 1 rack: 2 unidades
  *  Cabine de 2 racks: 2 unidades
  *  Cabine de 6 racks: 1 unidade
  *  Cabine de 8 racks: 1 unidade
  *  Switch de fibra: 1 unidade ( IC )
  *  Switch Hibrido: 1 unidade ( HC )
  *  Switch: 9 unidades
  *  Patch Panel: 9 unidades
  *  Patch Cords: 105 metros
  *  Access Point: 2 unidades
  *  Cabo fibra: 1.79 metros
  *  Cabo cobre: 41663 metros

### Piso 1 ###

Os HC's (Horizontal Cross Connect), CP's (Consolidation Points) e AP's (Access Points) foram colocados a 2.5 metros do solo, enquanto que os outlets estão situados no chão. 
A passagem dos cabos para as outras salas foi feita através do teto falso. Para esse efeito, foram necessários furos na infraestrutura.

O HC é constituido por um cabine de 1 rack que contém um switch híbrido de 24 portas que recebe o sinal fibra do IC e distribui um cabo de cobre para cada CP existente em cada sala do piso.

CP1 – Sala C0.1 – Constituído por uma cabine de dois racks, um patch panel de 24 portas, um switch de 24 portas, 12 metros de patch cords.

CP2 – Sala C0.2 – Constituído por uma cabine de dois racks, um patch panel de 24 portas, um switch de 24 portas, 12 metros de patch cords.

CP3 – Sala C0.3 – Constituído por uma cabine de dois racks, um patch panel de 24 portas, um switch de 24 portas, 12 metros de patch cords.

CP4 – Sala C0.4 – Constituído por uma cabine de dois racks, um patch panel de 24 portas, um switch de 24 portas, 12 metros de patch cords.

No total, foram utilizados para este piso 658 m de cabo. Foi considerado para as contas de cabo o plano elevado do posicionamento dos HC's e CP's.

Relativamente aos Access Poins:

  * Do Access Point 3 até o Horizontal Cross Connect são 83.5 metros de cabo.
  
Assim sendo:   

  * Piso 1 : 658 + 83.5 = 741.5 metros.


![Building C - Floor 1](Floor1C.png)


### Legenda ###


![Legenda](Legenda.jpg)


### Inventário ###
  *  Outlet: 66 unidades
  *  Switch Hibrido: 1 unidade ( HC )
  *  Switch: 4 unidades
  *  Cabine de 1 rack: 1 unidade
  *  Cabine de 2 racks: 4 unidades
  *  Patch Panel: 4 unidades
  *  Patch Cords: 48 metros
  *  Access Point: 1 unidade
  *  Cabo cobre: 741.5 metros