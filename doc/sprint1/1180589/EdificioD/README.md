# Edifício D - Eduardo Remelhe 1180589

Os cabos utilizados no edifício assim como no exterior são de cobre CAT6A (laranja) ou de fibra ótica (azul marinho). 
A quantidade de fibra ótica utilizada só foi usada de forma a possibilitar a conexão das diferentes partes que constituem o backbone (ligação entre IC e HCs e CPs). Desta forma o encargo a nível monetário da empresa não será elevado, uma vez que normalmente as empresas não se encontram equipadas com o hardware que permita a instalação deste tipo de fibra pois também é de difícil conexão e instalação. O edifício conta também com cobertura total de Wifi LAN.

A utilização de cabos de cobre CAT6A permite uma melhor conexão que o CAT6 pois o seu revestimento tem melhor resistência à possibilidade de radiação eletromagnética (comum quando existem interferências entre cabos). O facto de apresentar cabos cruzados, uma blindagem de alumínio e uma capacidade de 10Gbps torna-o uma escolha mais sustentável e com uma qualidade semelhante ao CAT7, a um menor preço.

## Edifício
![Edifício D](DoisPisosD.png)

### Cablagem estruturada utilizada
> Neste edifício é apenas utilizado um Horizontal Cross-Connect por piso. No primeiro piso o HC necessitará de um patch panel de 7 patch panels de 48 entradas e um de 24 entradas correpondendo a 72 switches ao qual acrescentamos 36 entradas para outlets e igual número de switches. Desta forma, ainda há possibilidade de acrescentar equipamento.
O HC do segundo piso contará com 2 patch panels um de 48 e outro de 24 com e um total de 56 entradas + 56 switches. 

### Cálculos: 10m equivale a 2.1cm

#Piso 0:
> D0.1 → 1.5cm x 1.6cm = 7.1m x 7.6m = 53.96m^2
> D0.2 → 1.5cm x 1.6cm = 7.1m x 7.6m = 53.96m^2
> D0.3 → 1.65cm x 1.6cm = 7.9m x 7.6m = 60.04m^2
> D0.4 → 1.4cm x 1.6cm = 6.7m x 7.6m = 50.92m^2
> Salão → (12cm x 1.2cm) + (4.2cm x 13.2cm) = (57.1m x 5.7m) + (4.2m x 13.2m) = 1583.47m^2

#Piso 1:
> D1.1 → 1.5cm x 1.6cm = 7.1m x 7.6m = 53.96m^2
> D1.2 → 1.5cm x 1.6cm = 7.1m x 7.6m = 53.96m^2
> D1.3 → 1.85cm x 2.3cm = 8.8m x 11m = 96.8m^2
> D1.4 → 1.05cm x 1.6cm = 5m x 7.6m = 38m^2

#Passagem Subterrânea: 15.2cm = 72.4m

### Outlets:
> Espaço D0.1 - área útil com 54 m2, necessita de 12 outlets.
> Espaço D0.2 - área útil com 54 m2, necessita de 12 outlets.
> Espaço D0.3 - área útil com 60 m2, necessita de 12 outlets.
> Espaço D0.4 - área útil com 51 m2, necessita de 12 outlets.
> Salão - área útil com 1584 m2, necessita de 316 outlets.
> Espaço D1.1 - área útil com 54 m2, necessita de 12 outlets.
> Espaço D1.2 - área útil com 54 m2, necessita de 12 outlets.
> Espaço D1.3 - área útil com 97 m2, necessita de 20 outlets.
> Espaço D1.4 - área útil com 38 m2, necessita de 6 outlets.

### Cablagem 
## Cobre
> Espaço D0.1
	> distância média - 11.25m 
	> total de cabos no espaço - 12.
        > total de comprimento - 135m

> Espaço D0.2
	> distância média - 11.25m 
	> total de cabos no espaço - 12.
        > total de comprimento - 135m

> Espaço D0.3
	> distância média - 6.5m 
	> total de cabos no espaço - 12.
        > total de comprimento - 78m

> Espaço D0.4
	> distância média - 11.25m 
	> total de cabos no espaço - 12.
        > total de comprimento - 135m

> Salão
	> distância média - 30m 
	> total de cabos no espaço - 320.
        > total de comprimento - 9600m

> Espaço D1.1
	> distância média - 11.25m 
	> total de cabos no espaço - 12.
        > total de comprimento - 135m

> Espaço D1.2
	> distância média - 11.25m 
	> total de cabos no espaço - 12.
        > total de comprimento - 135m

> Espaço D1.3
	> distância média - 18.75m 
	> total de cabos no espaço - 23.
        > total de comprimento - 431.25m

> Espaço D1.4
	> distância média - 22.5m 
	> total de cabos no espaço - 8.
        > total de comprimento - 180m

## Fibra-óptica
> Foi utilizada fibra-ótica na ligacao entre a vala externa e o IC, IC e o HC do mesmo piso e os HCs e os CPs totalizando 624m.


# Inventário
	> 10 964m de fio de cobre.
	> 624m de fibra ótica.
	> 423 outlets (368 no piso 0 + 55 no piso 1).
	> 1 Intermediate Cross-Connect.
	> 2 Horizontal Cross-Connect (1 para cada piso).
	> 10 Patch Panels( 8 no piso 0 + 2 no piso 1).
	> 7 Consolidation Points.
	> 3 dispositivos Wifi LAN (alcance de 25m cada um).