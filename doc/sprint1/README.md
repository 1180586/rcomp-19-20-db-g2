RCOMP 2019-2020 - Sprint 1
===========================================

# Edifício A #
No edifício A eram apresentados 2 pisos. No piso 0 são apresentadas 3 áreas úteis, das quais o espaço A0.1 é identificado pelo enunciado como sendo uma área que apenas necessitará de 5 outlets na mesa de entrada. Por outro lado, o espaço A0.2 e A0.3 necessitará de uma quantidade standard de outlets. Neste piso é apenas utilizado um Horizontal Cross-Connect que permitirá a ligação com o Intermediate Cross-Connect no piso superior de forma a se poder realizar a ligação de cabos de cobre pelas áreas de trabalho e áreas comuns por onde é necessária.

No piso 1 são apresentadas 3 áreas úteis, em que o espaço A1.1 é caracterizado como sendo a datacentre. O espaço A1.2, A1.3, A1.4 são apresentados como áreas de trabalho que apenas necessitam do número standard de outlets. Devido à apresentação de uma datacentre, é nesse local que se vai encontra o Intermediate Cross-Connect que possibilita a ligação dos 2 Horizontal Cross-Connects (piso 0 e 1) e que por si só irá ligar ao Main Cross-Connect que permitirá fazer a ligação entre os edifícios.

# Edifício B #

O planeamento do edificio B divide-se em dois pisos. Foi requisitada cobertura total de
wi-fi em ambos os pisos.  
O piso 0 é composto por 3 áreas úteis mais a service desk. A passagem de cabos é efetuada através de um canal no solo. O Intermediate Cross-Connect situa-se na entrance facility pois é a primeira sala por qual os cabos de fibra ótica passam.

O piso 1 é composto por 11 áreas úteis. A passagem de cabos é efetuada através de uma teto falso. Neste piso é apenas utilizado um Horizontal Cross-Connect que permitirá a ligação com o Intermediate Cross-Connect no piso inferior de forma a se poder realizar a ligação de cabos de cobre pelas áreas de trabalho e áreas comuns por onde é necessária.

# Edifício C #
O edifício C é composto por dois pisos. No primeiro, contamos com 5 áreas úteis. Sendo a sala C0.1 composta pela passagem de cabos para o piso superior e contendo também a passagem para o techinal ditch externo. A distribuição dos outlets pelas áreas úteis dos dois pisos está disposta nas seguintes tabelas:


|       Sala       |   C0.1      |  C0.2       |   C0.3  |   C0.4  |  C0.5   |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-------:|:-------:|
|Dimensões C x L (m)| 7.85 x 6.07 | 7.85 x 6.07 | 7.85 x 6.07 | 8.21 x 5.71 | 6.07 x 5.71 |
|Área(m2) | 47.65 | 47.65 |  47.65  | 46.87 | 34.66|
|Outlets | 10   | 10 | 10 | 10 | 8 |


|       Sala      |   C1.1      |  C1.2       |   C1.3  |   C1.4  |  C1.5   |
|:-----------------:|:-----------:|:-----------:|:-----------:|:-------:|:-------:|
|Dimensões C x L (m)| 5.94 x 6.75 | 5.94 x 6.75 | 5.94 x 10.81 | 5.94 x 10.81 |12.97 x 5.41 |
|Área(m2) | 40.10 | 40.10 |  64.20  | 79.80 | 70.16|
|Outlets | 10   | 10 | 14 | 16 | 16 |


## Piso 0 ##

 Neste piso todas as ligações de cabo foram efetuadas atraves de um Horizontal Cross Connect. Aas passagens de cabos para outras salas foi efetuada pelo teto falso, algo que implicou a existência de furos no teto.

 O Intermediate Cross-Connect é constituído por uma cabine de 1 rack que contém um switch de fibra de 8 portas que recebe 2 cabos de fibra ótica do MC ( exterior) e liga a cada HC dos pisos através de cabos de fibra ótica.

 O Horizontal Cross-Connect é constituido por um cabine de 1 rack que contém um switch híbrido de 24 portas que recebe o sinal fibra do IC e distribui um cabo de cobre para cada CP existente.

## Piso 1 ##

 A passagem dos cabos para as outras salas foi feita através do teto falso. Para esse efeito, foram necessários furos na infraestrutura.

 O Horizontal Cross-Connect é constituido por um cabine de 1 rack que contém um switch híbrido de 24 portas que recebe o sinal fibra do IC e distribui um cabo de cobre para cada CP existente em cada sala do piso.

# Edifício D #

No edifício D eram apresentados 2 pisos que necessitavam de cobertura de outlets e WIFI. O piso 0 com 4 áreas úteis e um salão com inúmeros cabos suspensos ambas. Na área D0.3 foi instalado um Horizontal Cross-Connect par distribuir para o piso todo enquanto que na área D0.4 foi instalado o Intermediate Cross-Connect do edifício, de forma a se poder realizar a ligação de cabos de cobre pelas áreas de trabalho e áreas comuns por onde é necessária.

No piso 1 são apresentadas também 4 áreas úteis. No espaço A1.3, foi instalado então o HC do piso em questao, mais uma vez com o objetivo de distribuir outlets por todas as áreas de trabalho. é também no espaço D1.4 que se faz a ligação do piso ao Intermediate Cross-Connect do edifício.

# Exterior #
No espaço exterior tivemos em conta a realização de cablagem em fibra ótica como forma de permitir uma ligação entre os vários edifícios no campus. Desta forma, a utilização de cabos de fibra ótica foi essencial, uma vez que é o tipo de cabo que deve ser utilizado na ligação entre os diferentes constituintes do backbone de cada edifício (devido à grande afluência de dados) e pela sua capacidade de conseguir valores mais altos de dados e um comprimento maior.
