RCOMP 2019-2020 - Planeamento Sprint 1
===========================================
### Sprint master: 1181486 ###

# 1. Sprint's backlog #
Neste trabalho era necessária a criação de um plano com uma estrutura de cablagem, envolvendo também todas as partes constituintes e necessárias para a realização do mesmo. 
Em cada edifício tivemos de realizar um plano de montagem de cabos que não só permitissem a ligação entre os diferentes edifícios mas também a ligação entre os diferentes constituintes de cada um (p.e. ligação entre MCs, HCs, ICs, CPs, outlets, entre outros). Também tivemos em atenção todo o material que seria necessário de instalar numa situação real como switches, patch panels, cabos de cobre e de fibra ótica.

# 2. Decisões técnicas #
  * Cabos de cobre CAT6A
  * Cabos de fibra ótica monomode
  * MC no Edifício A (datacentre)
  * IC em cada edifício
  * HCs nos pisos de cada edifício
  * utilização de CPs nos pisos com maior número de outlets
  * Wireless Access Points
  * Instalação de switches e patch panels na estrutura de cablagem do backbone

# 3. Atribuição de Tarefas #
  * 1180586 - Estrutura cablada Edifício A (incluindo a área externa que engloba todos os edifícios)
  * 1181488 - Estrutura cablada Edifício B
  * 1181486 - Estrutura cablada Edifício C
  * 1180589 - Estrutura cablada Edifício D
