

# Sprint 1 Inventário Geral #



## Inventário Individual de cada edifício ##

### Edificio A ###

* Cabo de cobre : 821.95 metros
* Cabo de Fibra Otica : 907.98 metros
* Outlets : 105 unidades
* Main Cross-Connect : 1 unidade
* Intermediate Cross-Connect : 1 unidade
* Horizontal Cross-Coneect : 2 unidades
* Patch Panels : 6 unidades

### Edificio B ###

* Cabo de cobre : 3038.72 metros
* Cabo de Fibra Otica : 153.32 metros
* Outlets : 198 unidades
* Wireless Acess Points: 3 unidades
* Consolidation Points: 13 unidades
* Intermediate Cross-Connect : 1 unidade
* Horizontal Cross-Coneect : 1 unidades
* Patch Panels : 4 unidades

### Edificio C ###

* Cabo de cobre : 42404.5 metros
* Cabo de Fibra Otica : 1.79 metros
* Outlets : 460 unidades
* Wireless Acess Points: 3 unidades
* Intermediate Cross-Connect : 1 unidade
* Horizontal Cross-Coneect : 2 unidades
* Patch Panels : 13 unidades
* Patch Cords : 153 metros
* Switch de fibra: 1 unidade ( IC )
* Switch Hibrido: 2 unidades ( HC )
* Switch : 13 unidades
* Cabine de 1 rack: 2 unidades
* Cabine de 2 racks: 2 unidades
* Cabine de 6 racks: 1 unidade
* Cabine de 8 racks: 1 unidade

### Edificio D ###

* Cabo de cobre :10964  metros
* Cabo de Fibra Otica : 624 metros
* Outlets : 423 outlets
* Wireless Acess Points: 3 unidades
* Consolidation Points: 7 unidades
* Intermediate Cross-Connect : 1 unidade
* Horizontal Cross-Coneect : 2 unidades
* Patch Panels : 10 unidades



## Inventário Total ##

* Cabo de cobre : 57229.17 metros
* Cabo de Fibra Otica : 1688.09 metros
* Outlets : 1186 unidades
* Wireless Acess Points: 9 unidades
* Intermediate Cross-Connect : 4 unidades
* Horizontal Cross-Coneect : 7 unidades
* Patch Panels : 33 unidades
* Patch Cords : 153 metros
* Switch de fibra: 1 unidade ( IC )
* Switch Hibrido: 2 unidades ( HC )
* Switch : 13 unidades
* Cabine de 1 rack: 2 unidades
* Cabine de 2 racks: 2 unidades
* Cabine de 6 racks: 1 unidade
* Cabine de 8 racks: 1 unidade
