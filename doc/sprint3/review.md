RCOMP 2019-2020 - Sprint 3 Review
=========================================
### Sprint master: 1181488 ###
# 1. Sprint's backlog #
# 2. Destribuição de tarefas #
Todos os membros do grupos terão de realizar a representenção e desenvolvimento a configuração de OSPF, Servidores HTTP e DNS, Telefones (VoIP), DHCP, NAT e ACL's nos seus respetivos edificios.. O aluno com o edifício B terá de no fim realizar a integração de todos os trabalhos realizados no Packet Tracer numa única simulação. Os dispositivos utilizados durante a realização do projeto devem ser as mesmas por todos os membros do grupo e devem respeitar as condições apresentadas no enunciado do Sprint. Procedeu-se à divisão das VLANs e IPv4 durante o processo de planning, de forma a promover uma melhor distribuição do trabalho pelos membros da equipa.

## 2.1. 1180586 - Configuração Campus Individual #
### Implementado com algumas falhas. ###
A implementação das mudanças do edifício A, tendo em conta o trabalho previamente realizado no Sprint 2, foi realizado corretamente coma exceção das ACLs que não foram executadas de acordo com o objetivo do Sprint.

## 2.2. 1180589 - Configuração Campus Individual #
### Implementado com algumas falhas.  ###
Todo o desenvolvimento efetuado no edifício B foi bem sucedido à excepção das ACL's que não foram corretamente executadas.

## 2.3. 1181486 - Configuração Campus Individual #
### Implementado com algumas falhas ###
Todo o processo de configuração e impletmentação do edifício C foi concretizado com sucesso exceptuante a implementação/configuração das ACL's.


## 2.4. 1181488 - Configuração Campus Individual #
###  Implementado com algumas falhas  ###
Sinto que o trabalho correu em boas condições, senti evolução ao longo do mesmo. Apenas falhei na configuração de ACL's.

## Campus Geral #
###  Implementado com bastantes falhas  ###
A nivel do campus, a junção dos 4 edificios foi realizada. No entanto com algumas falhas tendo em conta que não é possivel efetuar chamadas em telefones de diferentes edificios , não há possibilidade de consultar os sites nos servidores HTTP/DNS dos diferentes edificios nem foi possivel efetuar a configuração das ACL's.
