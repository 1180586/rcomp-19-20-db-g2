RCOMP 2019-2020 - Sprint 3 - Membro 1181488
===========================================

## **Introdução** ##
<p>
Neste sprint, cada membro da equipa vai continuar a trabalhar na mesma simulação do projeto anterior. Das já estabelecidas layer three configurations, agora OSPF dynamic routing vai ser usado para substituir static routing usado previamente.
</p>


### **Representação**

#### Edificio B

![EdB](EdB.JPG)

##### Valores Base

|   ----     | Nº Nodes | Somatório Nós | Tamanho Prefixo Network | Primeiro Endereço Válido | Tamanho Bloco Endereços | Endereço Utilizado (Network) | Intervalo ids a utilizar        |
| ------ | -------- | ------------- | ----------------------- | ------------------------ | ----------------------- | ---------------------------- | ------------------------------- |
| Piso 0 | 60       | 425           | /26                     | 10.165.118          | 64                      | 10.165.115.8               | 10.165.115.1 - 10.165.119.63 |
| Piso 1 | 70       |               | /25                     |                          | 128                      | 10.165.114.0               | 10.165.114.1 - 10.165.119.127  |
| WIFI   | 100       |               | /25                     |                          | 128                      | 10.165.114.128                 | 10.165.114.129 - 10.165.114.191    |
| DMZ    | 12       |               | /28                     |                          | 16                     | 10.165.115.128                 | 10.165.115.129 - 10.165.118.254   |
| VOIP   | 35       |               | /26                     |                          | 64                      | 10.165.115.64                | 10.165.115.64 - 10.165.115.255   |

##### VLAN's utilizadas no edifício

| VLAN (nome)     | VLAN (número) |
| --------------- | ------------- |
| VLAN\_B\_F0     | 110           |
| VLAN\_B\_F0     | 111           |
| VLAN\_B\_WIFI   | 112           |
| VLAN\_B\_SERVER | 114           |
| VLAN\_B\_VOIP   | 113           |


##### Endereços utilizados no edifício

| Dispositivo         | Endereço       | Mascara         | Default Gateway |
| ------------------- | -------------- | --------------- | --------------- |
| EDB\_0\_PC0         | 10.165.115.2   | 255.255.255.192 | 10.165.115.1    |
| EDB\_0\_PC1         | 10.165.115.3   | 255.255.255.192 | 10.165.115.1    |
| EDB\_1\_PC0         | 10.165.114.3   | 255.255.255.128 | 10.165.114.1    |
| EDB\_1\_PC1         | 10.165.114.2   | 255.255.255.128 | 10.165.114.1    |
| EDB\_0\_Laptop0     | 10.165.114.131 | 255.255.255.128 | 10.165.114.129  |
| EDB\_0\_Server0DNS  | 10.165.114.131 | 255.255.255.240 | 10.165.115.129  |
| EDB\_1\_Server1HTTP | 10.165.114.130 | 255.255.255.240 | 10.165.115.129  |
| EDB\_0\_IPPhone0    | 10.165.115.67  | 255.255.255.192 | 10.165.115.65   |
| EDB\_0\_IPPhone1    | 10.165.115.66  | 255.255.255.192 | 10.165.115.65   |

##### Endereço Backbone

| Backbone   | Ips utilizados | Máscara         | Intervalo Backbone            |
| ---------- | -------------- | --------------- | ----------------------------- |
| Backbone B  | 10.165.120.2   | 255.255.255.128 | 10.165.120.1 - 10.165.120.119 |

## **1. OSPF dynamic routing**

<p>
Static routing não será mais usado, assim em cada router, as static routing tables existentes foram apagadas sendo a única exceção o default router estabelecido no edificio A.
</p>
Toda a infraestrutura irá tornar-se um OSFP domain (Autonomous System), para isso teria de ser dividido em OSPF areas, uma para cada edificio e para o backbone (area id 0).
Ao edificio B foi atribuida a Area 2.



## **2. Servidores HTTP / DNS** ##
Para o servidor com a responsabilidade de ter a funcionalidade HTTP (EDB_1_Server1_HTTP) foi necessário realizar uma página HTML que demonstrasse qual era o a partir do qual era chamado.

![HTML](HTML.JPG)

## **3. DHCPv4**
A realização da configuração do DHCP foi realizada tendo em conta as VLANs dos PCs (ambos os pisos), telefones (VoIP phones) e laptop (WIFI).

##### VLAN PC/LAPTOP

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* domain-name rcomp-19-20-db-g2
* dns-server 10.165.115.131
* exit
* ip dhcp excluded-address IP_DO_ROUTER

##### VLAN VoIP

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* option 150 ip *DEFAULT_GATEWAY*
* exit
* ip dhcp excluded-address IP_DO_ROUTER

	* Os *NOME_POOL* utilizados foram os correspondentes às VLANs em que se estava a definir o DHCP.
	| NOME     | NÚMERO | EQUIPAMENTO CORRESPONDENTE|
	| --------------- | ------------- | |
	| VLAN110     | 110           | PCs do Piso 0 |
	| VLAN111   | 111           | PCs para o Piso 1 |
	| VLAN112     | 112           | Laptop |
	| VLAN113   | 113           | VoIP phones |

## **4. VoIP** #

##### Telefones Funcionais

![PHONE_1](PHONE_1.JPG)

![PHONE_2](PHONE_2.JPG)

| NOME  |  PISO  | NÚMERO |
|-------|--------|--------|
| EDB\_0\_IPPhone0  | 0  |	2000 |
| EDB\_0\_IPPhone1  |  1  | 2001	|

**Para a configuração do VoIP phones foram utilizados os seguintes comandos:**

###### Switch representativo do HCC do Piso 0 e 1

* enable
* configure terminal
* interface *INTERFACE*
* switchport mode access
* switchport voice vlan 113
* no switchport access vlan

	* As interfaces utilizadas foram as seguintes:
	| INTERFACE  |  PISO  |
	|------------|--------|
	| Fa5/1  | 1  |
	| Fa2/1  |  0 |

##### Router do Edifício B

* enable
* configure terminal
* telephony-service
* auto-reg-ephone
* ip source-address 10.165.115.65 port 2000
* max-ephones 2
* max-dn 2
* auto assign 1 to 2
* exit
* ephone-dn 1
* number 2000
* exit
* ephone-dn 2
* number 2001
* exit

	* O source-address utilizado é o default-gateway dos VoIP phones.

##### Configuração do router, de forma a fazer chamadas para outros edifícios
* enable
* configure terminal
* dial-peer voice 10 voip
* destination-pattern 1...
* session target ipv4:10.165.120.1
* exit
* dial-peer voice 30 voip
* destination-pattern 3...
* session target ipv4:10.165.120.3
* exit
* dial-peer voice 40 voip
* destination-pattern 4...
* session target ipv4:10.165.120.4
* exit

	* O valor do voip corresponde a um edifício específico, tendo em conta o endereço IP do backone desse mesmo edifício.
	| VOIP  |  ENDEREÇO BACKBONE  | EDIFICIO|
	|------------|--------|----------------|
	| 10  | 10.165.120.1  | A |
	| 30  |  10.165.120.3 | B |
	| 40  |  10.165.120.4 | C |

## **5. DNS**

| TIPO | NOME                         | VALOR                         |
|-------------|--------------------------------------|--------------------------------------|
| NS          | rcomp-19-20-db-g2         | ns.rcomp-19-20-db-g2      |
| NS          | building-b.rcomp-19-20-db-g2         | ns.building-b.rcomp-19-20-db-g2      |
| A           | ns.rcomp-19-20-db-g2      | 10.165.112.1                       |
| CNAME       | dns.building-b.rcomp-19-20-db-g2    | ns.building-b.rcomp-19-20-db-g2  |
| A           | server1 | 10.165.115.130                       |
| CNAME       | www.building-b.com    | server1 |
| CNAME       | web.building-b.com     | server1 |

##### Página WWW
![WWW](WWW.JPG)

##### Página WEB
![WEB](WEB.JPG)

## **6. NAT**

Os comandos utilizados para realizar a configuração da NAT, de acordo com os endereços dos servidores HTTP e DNS foram os seguintes:

* enable
* configue terminal
* interface Fa0/0.114 (VLAN SERVER)
* ip nat inside
* interface Fa0/0.130 (VLAN BACKBONE)
* ip nat outside
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 80 *ENDEREÇO_BACKBONE* 80
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 443 *ENDEREÇO_BACKBONE* 443
* ip nat inside source static tcp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53
* ip nat inside source static udp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53

| 	    ENDEREÇO      |  ENDEREÇO IP   |
|---------------------|----------------|
|    ENDEREÇO_IP_DNS  | 10.165.115.131   |
|    ENDEREÇO_IP_HTTP | 10.165.115.130   |
|   ENDEREÇO_BACKBONE | 10.165.120.2   |
