RCOMP 2019-2020 - Sprint 3 - Membro 1180586
===========================================
# Edifício A

O edifício A, neste projeto foi criado tendo como base o projeto realizado no Sprint 1. Tendo isto em conta, todas as ligações realizadas em cabos de cobre e fibra foram mantidas neste mesmo projeto. 

O projeto foi realizado tendo em conta a ligação à internet, a ligação aos edifícios constituintes do campus e a construção da rede de dispositivos dos pisos 0 e 1 do edifício.

Toda a estrutura utilizada para o desenvolvimento do Sprint 3 utilizou a represetação feita no Sprint 2, mantendo a dupla ligação para efeitos da STP.

O OSPF permite informar às áreas correspondentes e aos restantes routers sobre a network que se encontra associada aos dispositivos, desta forma suporta sistemas autónomos. Desta forma áreas OSPF são identificadas com um número que representa uma especificação específica e possibilita ter várias configurações de OSPF a serem utilizadas no mesmo router.

Relativamente à configuração do DHCPv4 foi necessário providenciar ligação entre as diferentes networks de cada edifício com a exceção das networks associadas aos DMZ e onde os endereços IPv4 são estáticos e configurados manualmente (routers e servidores). Esta configuração foi realizada de forma diferente para as networks dos PCs/Laptop e para os telefones disponíveis. Para os telefones tivemos em atenção o acrescento da configuração "option 150 ip ENDEREÇO_DEFAULT_GATEWAY", de forma a providenciar uma lista de endereços de vários servidores TFTP (permitem transferir dados de servidores utilizando User Data Protocol (UDP)).

O VoIP foi configurado de forma a permitir o reencaminhamento de chamadas no próprio edifício e entre diferentes edifícios. Para isso no edifício A foram utilizados 2 VoIP phones de forma a poder fazer o teste de realização de chamadas entre telefones do mesmo edifício, de forma a poder assegurar os mesmos. Na cli do router, ao qual os telefones deste edifício estavam ligados, foram adicionadas as restantes voip services que permitem realizar o teste de reencaminhamento de chamadas entre edifícios.

Neste Sprint o edifício A teve a responsabilidade de criar o DNS de domínio, que será utilizado como domínio raiz de todo o campus. Desta forma, todos os nomes de DNS utilizados nos restantes edifícios tiveram em conta a "base" do DNS criado no Edifício A, podendo ser geridos pelo domínio a que pertencem de forma autónoma. Para isso na tabela de DNS do router do edifício A foram adicionadas todas as ligações DNS feitas pelos restantes edifícios.

A realização da configuração da NAT teve em atenção os endereços dos servidores HTTP e DNS. A sua configuração permite a conservação dos endereços e impede que endereços indesejados consigam aceder à Internet. Normalmente realiza a ligação de duas networks e executa a conversão de endereços privados para endereços válidos, antes de os reencaminhar para outra network.

![EDA](EDA.PNG)

# Valores Base

|   ----     | Nº Nodes | Somatório Nós | Tamanho Prefixo Network | Primeiro Endereço Válido | Tamanho Bloco Endereços | Endereço Utilizado (Network) | Intervalo ids a utilizar        |
| ------ | -------- | ------------- | ----------------------- | ------------------------ | ----------------------- | ---------------------------- | ------------------------------- |
| Piso 0 | 40       | 194           | /26                     | 10.165.112.0/20          | 64                      | 10.165.112.128               | 10.165.112.129 - 10.165.112.191 |
| Piso 1 | 40       |               | /26                     |                          | 64                      | 10.165.112.192               | 10.165.112.93 - 10.165.112.254  |
| WIFI   | 24       |               | /27                     |                          | 32                      | 10.165.113.0                 | 10.165.113.1 - 10.165.113.31    |
| DMZ    | 70       |               | /25                     |                          | 128                     | 10.165.112.0                 | 10.165.112.1 - 10.165.112.127   |
| VOIP   | 20       |               | /27                     |                          | 32                      | 10.165.113.32                | 10.165.113.33 - 10.165.113.63   |

# VLAN's utilizadas no edifício

| VLAN (nome)     | VLAN (número) |
| --------------- | ------------- |
| VLAN\_A\_F0     | 115           |
| VLAN\_A\_WIFI   | 116           |
| VLAN\_A\_F1     | 117           |
| VLAN\_A\_VOIP   | 118           |
| VLAN\_A\_SERVER | 119           |
| VLAN\_BACKBONE  | 130           |


# Endereços utilizados no edifício

| DISPOSITIVO   	   | ENDEREÇO       | MÁSCARA         | DEFAULT GATEWAY |
| -------------------- | -------------- | --------------- | --------------- |
| EDA\_0\_PC0          | 10.165.112.129 | 255.255.255.192 | 10.165.112.190  |
| EDA\_0\_PC1          | 10.165.112.130 | 255.255.255.192 | 10.165.112.190  |
| EDA\_1\_Laptop       | 10.165.113.1   | 255.255.255.224 | 10.165.113.30   |
| EDA\_1\_PC2          | 10.165.112.193 | 255.255.255.192 | 10.165.112.254  |
| EDA\_1\_PC3          | 10.165.112.194 | 255.255.255.192 | 10.165.112.254  |
| EDA\_1\_IP\_Phone    | 10.165.113.34  | 255.255.255.224 | 10.165.113.62   |
| EDA\_1\_IP\_Phone\_2 | 10.165.113.33  | 255.255.255.224 | 10.165.113.62   |
| EDA\_1\_Server\_DNS  | 10.165.112.1   | 255.255.255.128 | 10.165.112.126  |
| EDA\_1\_Server\_HTTP | 10.165.112.2   | 255.255.255.128 | 10.165.112.126  |

# Endereço Backbone

| Backbone      | Ips utilizados | Máscara         | Intervalo Backbone            |
| ------------- | -------------- | --------------- | ----------------------------- |
| Backbone A    | 10.165.120.1   | 255.255.255.128 | 10.165.120.1 - 10.165.120.119 |
| Backbone Wifi | 10.165.120.5   | 255.255.255.128 |              -                |

# ISP

|          | Router ISP      |
| -------- | --------------- |
| Endereço | 17.10.10.89     |
| Máscara  | 255.255.255.252 |
|		   |(utilizado no ISP_Router e EDA_1_Router1)|

# OSPF

| DESIGNAÇÃ0 |  NETWORK  |   ÁREA   |
|------------|-----------|----------|
| PISO 0	| 10.165.112.190  | 1  |
| WIFI	| 10.165.113.30  | 1  |
| PISO 1	| 10.112.254  | 1  |
| VOIP PHONE	|  10.165.113.62 |  1 |
| SERVIDORES	| 10.165.112.126  | 1  |
| BACKBONE EDIFÍCIO A	|  10.165.120.1 | 0  |

Para a realização do DHCP foi necessário remover as tabelas de routing realizadas no Sprint anterior. O setup do OSPF utilizou os seguintes comandos na *cli* do Router do Edifício A:

* show ip interface brief
* configure terminal
* router ospf 1 
* network *ENDEREÇO_IP_NETWORK* 0.0.0.0 area *AREA_NETWORK* 

Este processo foi realizado para todas as networks do router do edifício.

# Servidor HTTP
No servidor que ficou com a responsabilidade de ter a funcionalidade HTTP (EDA_1_Server_HTTP) foi necessário realizar uma página HTML com uma frase representativa do edifício em questão.
![HTML](HTML.PNG)

# DHCPv4
A realização da configuração do DHCP foi realizada tendo em conta as VLANs dos PCs, telefones (VoIP phones) e laptop (WIFI).

## VLAN PC/LAPTOP

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* domain-name rcomp-19-20-db-g2
* dns-server 10.165.112.1
* exit
* ip dhcp excluded-address IP_DO_ROUTER

## VLAN VoIP

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* option 150 ip *DEFAULT_GATEWAY*
* exit
* ip dhcp excluded-address IP_DO_ROUTER

	* Os *NOME_POOL* utilizados foram os correspondentes às VLANs em que se estava a definir o DHCP.
	| NOME     | NÚMERO | EQUIPAMENTO CORRESPONDENTE|
	| --------------- | ------------- | |
	| VLAN115     | 115           | PCs para o Piso 0 |
	| VLAN116   | 116           | Laptop |
	| VLAN117     | 117           | PCs para o Piso 1 |
	| VLAN118   | 118           | VoIP phones |

# VoIP PHONE

## Telefones Funcionais

![PHONE_1](PHONE_1.PNG)

![PHONE_2](PHONE_2.PNG)

| NOME  |  PISO  | NÚMERO |
|-------|--------|--------|
| EDA\_1\_IP\_Phone  | 1  |	1000 |
| EDA\_1\_IP\_Phone\_2  |  0  | 1001	|

Para a configuração do VoIP phones foram utilizados os seguintes comandos:

## Switch representativo do HCC do Piso 0 e 1

* enable
* configure terminal
* interface *INTERFACE* 
* switchport mode access
* switchport voice vlan 295
* no switchport access vlan

	* As interfaces utilizadas foram as seguintes:
	| INTERFACE  |  PISO  | 
	|------------|--------|
	| Fa5/1  | 0  |
	| Fa1/1  |  1 | 

## Router do Edifício A

* enable
* configure terminal
* telephony-service
* auto-reg-ephone
* ip source-address 10.165.113.62 port 2000
* max-ephones 2
* max-dn 2
* auto assign 1 to 2
* exit
* ephone-dn 1
* number 1000
* exit
* ephone-dn 2
* number 1001
* exit

	* O source-address utilizado é o default-gateway dos VoIP phones.

## Configuração do router, de forma a fazer chamadas para outros edifícios
* enable
* configure terminal
* dial-peer voice 20 voip
* destination-pattern 2...
* session target ipv4:10.165.120.2
* exit
* dial-peer voice 30 voip
* destination-pattern 3...
* session target ipv4:10.165.120.3
* exit
* dial-peer voice 40 voip
* destination-pattern 4...
* session target ipv4:10.165.120.4
* exit

	* O valor do voip corresponde a um edifício específico, tendo em conta o endereço IP do backone desse mesmo edifício.
	| VOIP  |  ENDEREÇO BACKBONE  | 
	|------------|--------|
	| 20  | 10.165.120.2  |
	| 30  |  10.165.120.3 | 
	| 40  |  10.165.120.4 |

# DNS

| TIPO | NOME                         | VALOR                         |
|-------------|--------------------------------------|--------------------------------------|
| NS          | rcomp-19-20-db-g2         | ns.rcomp-19-20-db-g2      |
| A           | ns.rcomp-19-20-db-g2      | 10.165.112.1                       |
| CNAME       | dns.buildinga.rcomp-19-20-db-g2    | ns.rcomp-19-20-db-g2  |
| A           | server1.buildinga.rcomp-19-20-db-g2 | 10.165.112.2                       |
| CNAME       | www.buildinga.com    | server1.buildinga.rcomp-19-20-db-g2 |
| CNAME       | web.buildinga.com     | server1.buildinga.rcomp-19-20-db-g2 |
| NS          | building-b.rcomp-19-20-db-g2         | ns.building-b.rcomp-19-20-db-g2      |
| A           | ns.building-b.rcomp-19-20-db-g2      | 10.165.115.131                       |
| NS          | building-c.rcomp-19-20-db-g2         | ns.building-c.rcomp-19-20-db-g2     |
| A           | ns.building-c.rcomp-19-20-db-g2      | 10.165.116.2                       |
| NS          | building-d.rcomp-19-20-db-g2         | ns.building-d.rcomp-19-20-db-g2      |
| A           | ns.building-d.rcomp-19-20-db-g2      | 10.165.118.3                      |

## Página WWW
![WWW](WWW.PNG)

## Página WEB
![WEB](WEB.PNG)

# NAT

Os comandos utilizados para realizar a configuração da NAT, de acordo com os endreços dos servidores HTTP e DNS foram os seguintes:

* enable
* configue terminal
* interface Fa0/0.119 (VLAN SERVER) 
* ip nat inside
* interface Fa0/0.130 (VLAN BACKBONE)
* ip nat outside
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 80 *ENDEREÇO_BACKBONE* 80
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 443 *ENDEREÇO_BACKBONE* 443
* ip nat inside source static tcp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53
* ip nat inside source static udp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53

| 	    ENDEREÇO      |  ENDEREÇO IP   | 
|---------------------|----------------|
|    ENDEREÇO_IP_DNS  | 10.165.112.1   |
|    ENDEREÇO_IP_HTTP | 10.165.112.2   |
|   ENDEREÇO_BACKBONE | 10.165.120.1   |

## Teste NAT 80
![NAT_80](NAT_80.PNG)

## Teste NAT 443
![NAT_443](NAT_443.PNG)