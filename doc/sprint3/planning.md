RCOMP Projeto 2019-2020 - Planeamento Sprint 3
===========================================
### Sprint master: 1181488 ###

# 1. Sprint's Backlog #
Os membros do grupo vão continuar a trabalhar na simulação do projeto do sprint anterior. Da já estabelecida layer 3, agora OSPF based dynamic routing será usado para substituir static routing usado previamente. Para além de OSPF based dynamic routing, outras configurações relativas a este sprint são: serviço DHCPv4,VoIP, adicionar um segundo service a cada rede DMZ para operar o service de HTTP, configurar os servidoes de DNS, implementar NAT e estabelecer politicas relativas a tráfego nos routers.

# 2. Decisões Técnicas #
| Nome VLAN |  VLAN ID  |
|------------|-----------|
| 110 | VLAN_B_F0 |
| 111 | VLAN_B_F1 |
| 112 | VLAN_B_WIFI |
| 113 | VLAN_B_VOIP |
| 114 | VLAN_B_SERVER |
| 115 | VLAN_A_F0 |
| 116 | VLAN_A_WIFI |
| 117 | VLAN_A_F1 |
| 118 | VLAN_A_VOIP |
| 119 | VLAN_A_Server |
| 120 | VLAN_D_F0 |
| 121 | VLAN_D_F1 |
| 122 | VLAN_D_WIFI |
| 123 | VLAN_D_VOIP |
| 124 | VLAN_D_SERVER |
| 125 | VLAN_C_F0 |
| 126 | VLAN_C_F1 |
| 127 | VLAN_C_WIFI |
| 128 | VLAN_C_VOIP |
| 129 | VLAN_C_SERVER |
| 130 (Backbone) | VLAN_BB |

## Áreas
| Área |  Edifício  |
|------------|-----------|
| 0 | Backbone |
| 1 | A |
| 2 | B |
| 3 | C |
| 4 | D |

## Números Telefone
| Edifício |  Número  |
|------------|-----------|
| A | 1000/1001 |
| B | 2000/2001 |
| C | 3000/3001 |
| D | 4000/4001 |

## Voice VoIP (Chamadas entre edifícios)
| Edifício |  Número  |
|------------|-----------|
| A | 10 |
| B | 20 |
| C | 30 |
| D | 40 |

## Domínio DNS
| Edifício |  DNS  |
|------------|-----------|
| A (domain) | rcomp-19-20-db-g2 |
| B | building-B.rcomp-19-20-db-g2 |
| C | building-C.rcomp-19-20-db-g2 |
| D | building-D.rcomp-19-20-db-g2 |


# 3. Distribuição de Tarefas #

  * 1180586 - Atualizar o campus.pkt tendo em conta as mudanças realizadas na layer 3 do edifício A.
  * 1180589 - Atualizar o campus.pkt tendo em conta as mudanças realizadas na layer 3 do edifício D.
  * 1181486 - Atualizar o campus.pkt tendo em conta as mudanças realizadas na layer 3 do edifício C.
  * 1181488 - Atualizar o campus.pkt tendo em conta as mudanças realizadas na layer 3 do edifício B.
  * Equipa - Atualizar o campus.pkt tendo em conta todas as mudanças feitas individualmente.
