RCOMP 2019-2020 - Sprint 3 - Membro 1180589
===========================================
# Edifício D

![Edifício D](Edifício D.JPEG)

# Valores Base (presentes no Excel)

|   ----     | Nº Nodes | Somatório Nós | Tamanho Prefixo Network | Primeiro Endereço Válido | Tamanho Bloco Endereços | Endereço Utilizado (Network) |     Intervalo ids a utilizar    |
|  -------   | -------- | ------------- | ----------------------- | ------------------------ | ----------------------- | ---------------------------- | ------------------------------- |
|   Piso 0   |   40     |      425      |          /26            |      10.165.118.0/20     |          64             |       10.165.119.0           | 10.165.119.1 - 10.165.119.63    |
|   Piso 1   |   50     |               |          /26            |                          |          64             |       10.165.119.64          | 10.165.119.65 - 10.165.119.127  |
|   WIFI     |   60     |               |          /26            |                          |          64             |       10.165.119.128         | 10.165.119.129 - 10.165.119.191 |    |   DMZ      |   250    |               |          /24            |                          |          256            |       10.165.118.0           | 10.165.118.1 - 10.165.118.255   |
|   VOIP     |   20     |               |          /27            |                          |          32             |       10.165.118.192         | 10.165.119.193 - 10.165.119.255 |

# VLAN's utilizadas no edifício

| VLAN (nome)     | VLAN (número) |
| --------------- | ------------- |
| VLAN\_D\_F0     |      120      |
| VLAN\_D\_F1     |      121      |
| VLAN\_D\_WIFI   |      122      |
| VLAN\_D\_VOIP   |      123      |
| VLAN\_D\_SERVER |      124      |


# Endereços utilizados no edifício

| DISPOSITIVO          |    ENDEREÇO    |     MÁSCARA     | DEFAULT GATEWAY |
| -------------------- | -------------- | --------------- | --------------- |
| EDD\_0\_PC1          | 10.165.119.2   | 255.255.255.192 | 10.165.119.1    |
| EDD\_0\_PC2          | 10.165.119.3   | 255.255.255.192 | 10.165.119.1    |
| EDD\_0\_PC3          | 10.165.119.4   | 255.255.255.192 | 10.165.119.1    |
| EDD\_0\_PC4          | 10.165.119.5   | 255.255.255.192 | 10.165.119.1    |
| EDD\_1\_PC1          | 10.165.119.66  | 255.255.255.192 | 10.165.119.65   |
| EDD\_1\_PC2          | 10.165.119.67  | 255.255.255.192 | 10.165.119.65   |
| EDD\_Laptop          | 10.165.119.130 | 255.255.255.192 | 10.165.119.129  |
| EDD\_1\_Server\_DNS  | 10.165.118.2   | 255.255.255.0   | 10.165.118.1    |
| EDD\_1\_Server\_HTTP | 10.165.118.3   | 255.255.255.0   | 10.165.118.1    |
| EDD\_1\_IPPhone1     | 10.165.119.194 | 255.255.255.224 | 10.165.119.193  |
| EDD\_1\_IPPhone2     | 10.165.119.195 | 255.255.255.224 | 10.165.119.193  |


# Endereço Backbone

| Backbone      | Ips utilizados |     Máscara     |      Intervalo Backbone       |
| ------------- | -------------- | --------------- | ----------------------------- |
| Backbone D    | 10.165.120.4   | 255.255.255.128 | 10.165.120.1 - 10.165.120.119 |


# DHCP

Para a realização do DHCP foi necessário remover as tabelas de routing realizadas no Sprint anterior. O setup utilizou os seguintes comandos na *cli* do Router do Edifício D:

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* domain-name buildin-D.rcomp-19-20-db-g2
* dns-server 10.165.112.193
* exit
* ip dhcp excluded-address IP_DO_ROUTER

![Show r das VLANS](showrVLANS.JPEG)


## VLAN VoIP

* ip dhcp pool *NOME_POOL*
* network *ENDEREÇO_IP_NETWORK* *MASCARA_DA_REDE*
* default-router *DEFAULT_GATEWAY*
* option 150 ip *DEFAULT_GATEWAY*
* exit
* ip dhcp excluded-address IP_DO_ROUTER
* Os *NOME_POOL* utilizados foram os correspondentes às VLANs em que se estava a definir o DHCP.
	
| NOME     | NÚMERO | EQUIPAMENTO CORRESPONDENTE |
| -------- | ------ | -------------------------- |
| VLAN120  | 120    | PCs para o Piso 0          |
| VLAN121  | 121    | PCs para o Piso 1          |
| VLAN122  | 122    | LAPTOP                     |
| VLAN123  | 123    | VoIP phones                |
| VLAN124  | 124    | Servers                    |


# VoIP PHONE

## Telefones Funcionais

| NOME           |  PISO  | NÚMERO |
| -------------- |--------|--------|
| EDD\_IPPhone1  | 1      | 4000   |
| EDd\_IPPhone2  | 1      | 4001   |

Para a configuração do VoIP phones foram utilizados os seguintes comandos:

## Switch representativo do HCC do Piso 1

* enable
* configure terminal
* interface *INTERFACE* 
* switchport mode access
* switchport voice vlan 123
* no switchport access vlan


## Router do Edifício D

* enable
* configure terminal
* telephony-service
* auto-reg-ephone
* ip source-address 10.165.119.193 port 2000
* max-ephones 2
* max-dn 2
* auto assign 1 to 2
* exit
* ephone-dn 1
* number 4000
* exit
* ephone-dn 2
* number 4001
* exit


## Configuração do router, de forma a fazer chamadas para outros edifícios
* enable
* configure terminal
* dial-peer voice 10 voip
* destination-pattern 1...
* session target ipv4:10.165.120.1
* exit
* dial-peer voice 20 voip
* destination-pattern 2...
* session target ipv4:10.165.120.2
* exit
* dial-peer voice 30 voip
* destination-pattern 3...
* session target ipv4:10.165.120.3
* exit

![Show r das Chamadas](dialPeerVOIP.JPEG)


# DNS

| NOME                              | TIPO     | DETAIL                          |
|---------------------------------- | -------- | ------------------------------- |
| building-d.rcomp-19-20-db-g2      | NS       | ns.building-d.rcomp-19-20-db-g2 |
| dns.building-d.rcomp-19-20-db-g2  | CNAME    | ns.building-d.rcomp-19-20-db-g2 |
| ns.building-d.rcomp-19-20-db-g2   | A Record | 10.165.118.3                    |
| ns.rcomp-19-20-db-g2              | A Record | 10.165.112.1                    |
| rcomp-19-20-db-g2                 | NS       | ns.rcomp-19-20-db-g2            |
| server1                           | A Record | 10.165.118.2                    |
| web.building-d.com                | CNAME    | server1                         |
| www.building-d.com                | CNAME    | server1                         |

![Tabela DNS](tabelaDNS.JPEG)
    

# NAT

Os comandos utilizados para realizar a configuração da NAT, de acordo com os endreços dos servidores HTTP e DNS foram os seguintes:

* enable
* configue terminal
* interface Fa0/0.124 (VLAN SERVER) 
* ip nat inside
* interface Fa0/0.130 (VLAN BACKBONE)
* ip nat outside
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 80 *ENDEREÇO_BACKBONE* 80
* ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 443 *ENDEREÇO_BACKBONE* 443
* ip nat inside source static tcp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53
* ip nat inside source static udp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53

| 	ENDEREÇO      |  ENDEREÇO IP   | 
|---------------------|----------------|
|   ENDEREÇO_IP_DNS   |  10.165.119.69 |
|   ENDEREÇO_IP_HTTP  |  10.165.119.68 |
|   ENDEREÇO_BACKBONE |  10.165.120.4  |
