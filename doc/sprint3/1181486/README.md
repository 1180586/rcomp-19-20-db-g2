#Edificio C - Carlos Daniel Santos Pinto Ferreira - 1181486


De forma a priorizar a coesão do projeto, o edifício C respeita todas as decisões tomadas nos sprints anteriores.
Desta feita, todas as ligações realizadas com cabos de cobre e fibra foram respeitadas e mantidas no presente projeto.

Numa instancia inicial da realização do projeto, tomei como iniciativa omitir todos os CP's representados no Sprint anterior de forma a garantir uma apresentação simplificada e esteticamente mais minimalista do Edificio em questão.

Assim sendo, utilizei PT-EMPTY SWITCHES de forma a representar os Cross-Connects (Main Cross Connect, Intermediate Cross Connect, Horizontal Cross Connect) e os Consolidation Points.

Desta feita, os PT-EMPTY SWITCHES denominados por EDC_1_CP1, EDC_1_CP2, EDC_1_CP3 tem como função, simular todos os Consolidation Points do Edificio C.

A estes Switches, efetuei ligações aos diferentes dispositivos requiridos no enunciado (PC's, Laptop's, IPPhone's, Server's).

A utilização de um Router ligado ao Intermediate Cross Connect do presente Edificio, permitiu que se encontrassem nesse dispositivo de rede apenas as VLAN ID's e respetivos endereços que fossem cruciais para a execução de uma ligação do Switch ao End User, permitindo assim uma maior percepção da sua localização e transmissão.

Dessa forma, ao executar as ligações dos Switches em Trunk Mode, a propagassão dessas VLAN's por todos os outros Switches era asseguranda. Permitindo assim acessar aos dispositivos dos quais se pretendia fazer ligação.

Realizei uma ligação do Main Cross Connect, representado por um Switch, aos diferentes Intermediate Cross Connects dos diversos edifícios de forma a simular uma representação fidedigna de uma ligação ágil e efetiva entre os diferentes edifícios.




## *1. Representação*  ##
![EDC_S3](EDC_S3.JPG)


### *VLAN's* ###

No presente edificio foram consideradas as seguintes VLAN's:

| Nome VLAN | Numero VLAN |
|:---------:|:-----------:|
|VLAN_C_F0 | 125|
|VLAN_C_F1 | 126|
|VLAN_C_WIFI| 127|
|VLAN_C_VOIP| 128|
|VLAN_C_SERVER| 129|


### *Piso 0* ###

No presente piso foram considerados os seguintes dispositivos com os seguintes IP's:

| Nome dispositivo |  Endereço | Mascara | Default Gateway  |
|:----------------:|:---------:|:-------:|:----------------:|
| EDC_0_PC1 | 10.165.117.2 | 255.255.255.192 | 10.165.117.1 |
| EDC_0_PC2 | 10.165.117.3 | 255.255.255.192 | 10.165.117.1 |
|EDC_0_IPPhone1|     -     |        -        |      -       |
|EDC_0_AC|           -     |        -        |      -       |
| EDA_0_DNS_Server | 10.165.116.2 | 255.255.255.0 | 10.165.116.1|
| EDC_o_Laptop| 10.165.117.130 | 255.255.255.192 | 10.165.117.129|


### *Piso 0* ###

No presente piso foram considerados os seguintes dispositivos com os seguintes IP's:


| Nome dispositivo |  Endereço | Mascara | Default Gateway|
|:----------------:|:---------:|:-------:|:--------------:|
|EDC_1_PC1 | 10.165.117.66 | 255.255.255.192 |10.165.117.65|
|EDC_1_PC2 | 10.165.117.67 | 255.255.255.192 |10.165.117.65|
|EDC_1_PC3| 10.165.117.68 | |255.255.255.192 | 10.165.117.65|
|EDC_1_PC4| 10.165.117.69 | 255.255.255.192 | 10.165.117.65|
|EDC_1_PC5| 10.165.117.70 | 255.255.255.192| 10.165.117.65|
|EDC_1_HTTP_SERVER| 10.165.116.3 |255.255.255.0 |10.165.116.1|
|EDC_1_IPphone1| -   | -   |   -  |


### *1. Configuração OSPF* ###

| DESIGNAÇÃ0 |  NETWORK  |   ÁREA   |
|------------|-----------|----------|
| PISO 0	| 10.165.112.190  | 1  |
| WIFI	| 10.165.113.30  | 1  |
| PISO 1	| 10.112.254  | 1  |
| VOIP PHONE	|  10.165.113.62 |  1 |
| SERVIDORES	| 10.165.112.126  | 1  |
| BACKBONE EDIFÍCIO A	|  10.165.120.1 | 0  |

Para a realização do DHCP foi necessário remover as tabelas de routing realizadas no Sprint anterior. O setup do OSPF utilizou os seguintes comandos na *cli* do Router do Edifício A:

* ** show ip interface brief **
* ** configure terminal **
* ** router ospf 1 **
* ** network *ENDEREÇO_IP_NETWORK* 0.0.0.0 area *AREA_NETWORK1* **

Este processo foi realizado para todas as networks do router do edifício.


### *2. Servidores HTTP / DNS* ###

Ao presente edificio foi considerado um servidor como responsavel pelo DNS da rede e foi adicionado um outro servidor como responsavel pelo HTTP da rede.
Em anexo segue-se a imagem representativa da janela com as devidas "configurações" do servidor DNS:

![DNS_SERVER](DNS_SERVER.JPG)


Em seguida está representada a janela com as "configurações do servidor HTTP":

![HTTP_SERVER](HTTP_SERVER.JPG)


### *3. DHCPv4* ###

O edificio em questão terá 4 VLANS com DHCP configurado no próprio Router.
As VLAN's em questão são:

* VLAN_C_F0
* VLAN_C_F1
* VLAN_C_WIFI

Os comandos a efetuar em cada VLAN, serão, a partir do modo de configuração global:

* ** ip dhcp pool *NOME_DA_POOL* **
* ** network *ENDERECO_DE_REDE* *MASCARA_DA_REDE* **
* ** default-router *IP_DO_ROUTER* **
* ** domain-name *ENDERECO_A_DEFINIR_NO_PONTO_5* **
* ** dns-server *IP_DO_SERVER_DE_DNS* **
* ** exit **
* ** ip dhcp excluded-address *IP_DO_ROUTER* **



| NOME DA POOL | ENDEREÇO DA REDE | MASCARA DA REDE | IP DO ROUTER | ENDEREÇO A DEFINIR | IP DO SERVIDOR DE DNS |
|:------------:|:----------------:|:---------------:|:------------:|:------------------:|:---------------------:|
| VLAN125 | 10.165.117.0 | 255.255.255.192 | 10.165.117.1 | building-C.rcomp-19-20-db-g2 |10.165.116.2 |
| VLAN126 | 10.135.117.64 | 255.255.255.192 |10.165.117.65 | building-C.rcomp-19-20-db-g2 |10.165.116.2 |
| VLAN127 | 10.165.117.128 | 255.255.255.192 | 10.165.117.129 | building-C.rcomp-19-20-db-g2 | 10.165.116.2|



No caso da *VLAN VoIP*, a configuração será feita da seguinte forma:

* ** ip dhcp pool *VOIP_POOL* **
* ** network *ENDERECO_DE_REDE* *MASCARA_DA_REDE* **
* ** default-router *IP_DO_ROUTER* **
* ** option 150 ip *IP_DO_ROUTER* **
* ** exit **
* ** ip dhcp excluded-address *IP_DO_ROUTER* **



| NOME DA POOL | ENDEREÇO DA REDE | MASCARA DA REDE | IP DO ROUTER |
|:------------:|:----------------:|:---------------:|:------------:|
 |VLAN128 | 10.165.117.192 | 255.255.255.192 |10.165.117.193 |



### *4. VoIP* ###

Nas **Portas dos Switches ligados aos telefones** foram colocados os seguintes comandos:


* ** int *INTERFACE_EM_QUESTÃO* **
* ** switchport mode access **
* ** switchport voice vlan *NR_VLAN* **
* ** no switchport access vlan **

| INTERFACE | PISO |
|:---------:|:----:|
| Fa6/1 | 0 |
| Fa3/1 | 1 |

Como já **configuramos o DHCP** anteriormente, agora só temos de configurar o serviço de telefonia através dos comandos em baixo descritos.


## Router do Edificio C ##

* ** telephony-service **
* ** auto-reg-phone **
* ** ip source-address *IP_DO_ROUTER* port 2000 **
* ** max-ephones *NR_DE_TELEFONES* **
* ** max-dn *NR_DE_TELEFONES* **
* ** auto assign 1 to *NR_DE_TELEFONES* **
* ** exit **
* ** ephone-dn 1 **
* ** number *NUMERO_DO_PRIMEIRO_TELEFONE* **
* ** ephone-dn 2 **
* ** number *NUMERO_DO_SEGUNDO_TELEFONE* **
* ** exit **

| NOME DO TELEFONE | PISO | NÚMERO DE TELEFONE |
|:----------------:|:----:|:------------------:|
| EDC_0_IPPhone1  | 0 | 3000|
| EDC_1_IPphone1 | 1 | 3001|

## Teste à funcionalidade de efetuar chamada entre telefones

A imagem exposta representa o teste efetuado ao efetuar uma chamada entre dois telefones do mesmo edificio.

![TESTE_CHAMADA](TESTE_CHAMADA.JPG)


## Configuração do router, de forma a fazer chamadas para outros edifícios

* ** dial-peer voice *TAG* voip **
* ** destination pattern XXXXX **
* ** session target ipv4:*IP_DO_OUTRO_EDIFICIO_NA_BACKBONE* **

| TAG | DESTINATION PATTERN | IP DA BACKBONE EDIFICIO |
|:---:|:-------------------:|:-----------------------:|
| 10 | 1... | 10.165.120.1 |
| 20 | 2... | 10.165.120.2 |
| 40 | 4... | 10.165.120.4 |



### *5.DNS* ###

| TIPO | NOME | VALOR |
|:----:|:----:|:-----:|
| NS | rcomp-19-20-db-g2 | ns.rcomp-19-20-db-g2 |
| A RECORD | ns.rcomp-19-20-db-g2 | 10.165.112.1 |
| NS | building-c.rcomp-19-20-db-g2 | ns.building-c.rcomp-19-20-db-g2 |
| A RECORD | ns.building-c.rcomp-19-20-db-g2 | 10.165.116.2 |
| CNAME | dns.building-c.rcomp-19-20-db-g2 | ns.building-c.rcomp-19-20-db-g2|
| A RECORD | server1 | 10.165.116.3 |
| CNAME | www.building-c.com | server1 |
| CNAME | web.building-c.com | server1 |

## Página WWW
![WWW_PAGE](WWW_PAGE.JPG)

## Página WEB
![WEB_PAGE](WEB_PAGE.JPG)

### *6. NAT* ###

Os comandos utilizados para realizar a configuração da NAT, de acordo com os endreços dos servidores HTTP e DNS foram os seguintes:

* ** enable **
* ** configue terminal **
* ** interface Fa0/0.119 (VLAN SERVER) **
* ** ip nat inside **
* ** interface Fa0/0.130 (VLAN BACKBONE) **
* ** ip nat outside **
* ** ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 80 *ENDEREÇO_BACKBONE* 80 **
* ** ip nat inside source static tcp *ENDEREÇO_IP_HTTP* 443 *ENDEREÇO_BACKBONE* 443 **
* ** ip nat inside source static tcp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53 **
* ** ip nat inside source static udp *ENDEREÇO_IP_DNS* 53 *ENDEREÇO_BACKBONE* 53 **

| 	    ENDEREÇO      |  ENDEREÇO IP   |
|---------------------|----------------|
|    ENDEREÇO_IP_DNS  | 10.165.116.2   |
|    ENDEREÇO_IP_HTTP | 10.165.116.3   |
|   ENDEREÇO_BACKBONE | 10.165.120.3   |

## Teste NAT TCP 80

Imagem representativa do resultado ao teste efetuado à configuração NAT (80).

![TESTE_NAT_80](TESTE_NAT_80.JPG)


## Teste NAT TCP 443

Imagem representativa do resultado ao teste efetuado à configuração NAT (443).

![TESTE_NAT_443](TESTE_NAT_443.JPG)
